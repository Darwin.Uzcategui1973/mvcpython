import json

class Personas(object):
    # Contructor de la clases Persona
    def __init__(self,nombre=None, apellido=None):
        self.nombre=nombre
        self.apellido=apellido
        
    # metdodo de clases returna Nombre de persona Ejm Darwin Uzcategui
    def nombre_apellido(self):
        #print(self.nombre)
        return("%s %s" %(self.nombre, self.apellido))

    @classmethod
    #returna  todas las personas dentro de la basedatos BBDD.txt y lista json objetos Personas
    def getAll(self):
        BBDD= open("BBDD.TXT","r")
        resultado=[]
        
        
        json_lista=json.loads(BBDD.read())
      
        for item in json_lista:
            
            persona=Personas(item["nombre"],item["apellido"])
                      
            resultado.append(persona.nombre_apellido())
        
        return resultado
    

