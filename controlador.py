from modelo import Personas
import vista

def verTodo():
    # gets lista de todas las personas del objeto
    persona_en_BBDD=Personas.getAll()
    # aqui luego llama a la vista
    return vista.verTodoVista(persona_en_BBDD)

def comienza():
    vista.cominezaVista()
    opcion=input()
    if opcion=="s":
        #nombre_apellido()
        return verTodo()
    else:
        return vista.finalVista()

if __name__ =="__main__":
    # comienza a correr el controlador
    comienza()